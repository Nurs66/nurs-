from django.db import models
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver

User = get_user_model()


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="profile")
    name = models.CharField(max_length=100)
    tag = models.ManyToManyField('Tag')

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class Company(models.Model):
    name = models.CharField(max_length=100)
    owner = models.ManyToManyField(Profile, related_name="company")

    def __str__(self):
        return self.name

    def accept_request(self, id_user):
        request = Request.objects.get(profile=id_user)
        request.active = True
        request.save()
        request.checking()
        return request.active


class EmployesInCompany(models.Model):
    employee = models.ManyToManyField(Profile)
    company = models.OneToOneField(Company, on_delete=models.CASCADE, related_name="employes")

    def __str__(self):
        return self.company.name


class Request(models.Model):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name="requests")
    active = models.BooleanField(default=False)

    def __str__(self):
        return self.profile.name + self.company.name

    def checking(self):
        if self.active:
            self.company.employes.employee.add(self.profile)
            return self.active



class Tag(models.Model):
    title = models.CharField(max_length=50)

    def __str__(self):
        return self.title
